function init() {
  /**
   * Convierte el JSON a string para poder mostrarlo
   * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
   */
  var JsonString = JSON.stringify(baseJSON,null,2);
  document.getElementById("description").value = JsonString;

  listarProductos();
}


function listarProductos() {
  $.ajax({
      url: 'backend/product-list.php',
      type: 'GET',
      success: function(response) {
          const products = JSON.parse(response);
          let template = '';
          products.forEach(product => {
            // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
              let descripcion = '';
              descripcion += '<li>precio: '+product.precio+'</li>';
              descripcion += '<li>unidades: '+product.unidades+'</li>';
              descripcion += '<li>modelo: '+product.modelo+'</li>';
              descripcion += '<li>marca: '+product.marca+'</li>';
              descripcion += '<li>detalles: '+product.detalles+'</li>';

              template += `
                      <tr taskId="${product.id}">
                      <td>${product.id}</td>
                      <td>
                      <a href="#" class="task-item">
                      ${product.nombre} 
                      </a>
                      </td>
                      <td>${product.detalles}</td>
                      <td><ul>${descripcion}</ul></td>
                      <td>
                      <button class="task-delete btn btn-danger">
                      Eliminar
                      </button>
                      </td>
                      </tr>
                  `
          });
          $('#tasks').html(template);
      }
  });
}



$(document).ready(function() {

  let edit = false;
  listarProductos();
  $('#task-result').hide();
  
  // search key type event
  $('#search').keyup(function() {
      if($('#search').val()) {
          let search = $('#search').val();
          $.ajax({
              url: './backend/product-search.php',
              data: {search},
              type: 'GET',
              success: function (response) {
                if(!response.error){
                  let tasks = JSON.parse(response);
                  let template = '';
                  let template_bar = '';
                  $("#tasks").empty();
                  tasks.forEach(task => {
                    let descripcion = '';
                    descripcion += '<li>precio: '+task.precio+'</li>';
                    descripcion += '<li>unidades: '+task.unidades+'</li>';
                    descripcion += '<li>modelo: '+task.modelo+'</li>';
                    descripcion += '<li>marca: '+task.marca+'</li>';
                    descripcion += '<li>detalles: '+task.detalles+'</li>';
                
                    template += `
                        <tr taskId="${task.id}">
                            <td>${task.id}</td>
                            <a href="#" class="task-item">
                            <td>${task.nombre}</td> </a>
                            <td><ul>${descripcion}</ul></td>
                            <td>
                            <button class="task-delete btn btn-danger">
                            Eliminar 
                            </button>
                            </td>
                        </tr>
                    `;
        
                    template_bar += `
                        <li>${task.nombre}</li>
                    `;
                    
                  });
                  $('#task-result').show();
                  $('#container').html(template_bar);
                 $('#tasks').html(template);
                }
                                     
              } 
          })
      }
  });
  
  
  $('#task-form').submit(e => {
    e.preventDefault();
    var formulario = [];
    let productoJsonString = $('#description').val();
    let finalJSON = JSON.parse(productoJsonString);
    finalJSON['nombre'] = $("#name").val();
    let finalJsonString = JSON.stringify(finalJSON,null,2);
    const url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';
    JSON.parse(finalJsonString, (key, value) => {
        formulario[key] = value;
    })

    if ( validarDatos(formulario) && edit == false) {
        $.post(url, {finalJsonString}, (response) => {
        let respuesta = JSON.parse(response);
        let template_bar = '';
        template_bar += `
            <li style="list-style: none;">status: ${respuesta.status}</li>
            <li style="list-style: none;">message: ${respuesta.message}</li>
        `;
        listarProductos();
        $('#task-result').show();
        $('#container').html(template_bar);
        $('#task-form').trigger('reset');
        alert("Los datos son válidos");
        listarProductos();
       });
    } 
    else
        alert("Los datos son incorrectos");
    listarProductos();
});



  // Get a Single product by Id 
  $(document).on('click', '.task-item', (e) => {
    const element = $(this)[0].activeElement.parentElement.parentElement;
    const id = $(element).attr('taskId'); 
    $.post('backend/product-single.php', {id}, (response) => {
        const tasks = JSON.parse(response);
        $('#name').val(tasks[0].nombre);
        var JsonString = JSON.stringify(tasks,null,2);
        document.getElementById("description").value = JsonString;
        edit = true;
        $("#btn-submit").text("Modificar");
        
    });
    e.preventDefault();
});


  // Delete a Single product
  $(document).on('click', '.task-delete', function(){
    if(confirm('Esta seguro de eliminar este producto?')){
      let element = $(this)[0].parentElement.parentElement;
      let id = $(element).attr('taskId');
      $.get('./backend/product-delete.php', {id}, (response) => {
        let respuesta = JSON.parse(response);
        let template_bar = '';
        template_bar += `
            <li style="list-style: none;">status: ${respuesta.status}</li>
            <li style="list-style: none;">message: ${respuesta.message}</li>
        `;
        listarProductos();
        $('#task-result').show();
        $('#container').html(template_bar);
      });
    }
  });

// Busca el nombre en la base de datos
$('#name').keyup(function() {
  if($('#name').val()) {
      let searchN = $('#name').val();
      $.ajax({
          url: './backend/product-searchN.php?nombre='+$('#name').val(),
          data: {searchN},
          type: 'GET',
          success: function (response) {
            if(!response.error){
              $('#container2').html("ESTE NOMBRE SI ESTA EN LA BASE DE DATOS");
            }else{
              $('#container2').html("ESTE NOMBRE NO ESTA EN LA BASE DE DATOS");
            }                         
          }
      }) //termina ajax
  }
}); 


  
});

//Validaciones 
//se obtiene todos los elementos que contiene el formulario
const formulario = document.getElementById('task-form');
const inputs = document.querySelectorAll('#task-form input');
const select = document.querySelectorAll('#task-form select');

//Las expresiones regulares serviran para poder realizar de manera satisfactoria las validaciones
const expresiones = {
	//se debe tener cuidado al poner que tipo de caracteres contiene ya que causaria que en el formulario no sirvan
	nombre: /^[a-zA-ZÀ-ÿ0-9\s]{1,100}$/, // Letras,espacios,numeracion del 0 al 9, pueden llevar acentos y de 1 a 100.
	modelo: /^[a-zA-Z0-9\s_.+-]{1,25}$/, // 1 a25 carcateres y ademas es alfanumerico y sin espacios.
	detalles: /^\d{0,250}$/, //debe contener almenos 250 caracteres
	precio:  /^[0-9]+.[0-9][0-9]$/, //contiene numeros
	marca: /Xiaomi|Realme|Umidigi|Motorola|Samsung|Iphone|VIVO|TCL|TecnoSpark/,
    unidades: /^[0-9]{1,}$/ //numeros del 0 al 9 y numeros formados de 1 a 5 digitos
}

//serviran de guia para poder enviar el formulario por una que se encuentre en false
//este no se enviara
const campos = {
	nombre: false,
	modelo: false,
	detalles: false,
	marca: false,
	unidades: false
}

const validarFormulario = (e) => {
	switch (e.target.name) {
		case "nombre":
		validarCampo(expresiones.nombre, e.target, 'nombre');
        validarNombre();
		break;
		case "marca":
		validarCampo(expresiones.marca, e.target, 'marca');
        validarMarca();
		break;
		case "modelo":
		validarCampo(expresiones.modelo, e.target, 'modelo');
        validarModelo();
		break;
		case "precio":
		validarCampo(expresiones.precio, e.target, 'precio');
		validaPrecio();
		break;
		case "detalles":
		validarCampo(expresiones.detalles, e.target, 'detalles');
		validaDetalles();
		break;
		case "unidades":
		validaUnidad();
		validarCampo(expresiones.unidades, e.target, 'unidades');
		break;
	}
}


//validacion general para el llenado de los campos
//esto quiere decir que debe de cumplir con la validaciones delas expresiones regulares
const validarCampo = (expresion, input, campo) => {
	if(expresion.test(input.value)){
		document.getElementById(`grupo__${campo}`).classList.remove('task-form__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo__${campo} .task-form__input-error`).classList.remove('task-form__input-error-activo');
        $('#container').html("Campo correcto");
		campos[campo] = true;
	} else {
		document.getElementById(`grupo__${campo}`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle')
		document.querySelector(`#grupo__${campo} .task-form__input-error`).classList.add('task-form__input-error-activo');
		$('#container').html("Campo incorrecto");
		campos[campo] = false;
	}
}

let template = '';
//muy aparte se hace una copia de las validaciones generales por si se requieres una validacion especial
//por ejemplo que el campo cumpla que no este vacio y ptras reglas descritas 
const validarMarca = () =>{
	const marcaP = document.getElementById('marca');
	//var marcaP = document.getElementById('marca');
	
	if(marcaP.value == null || marcaP.value ==""){
		document.getElementById(`grupo__marca`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__marca`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__marca .task-form__input-error`).classList.add('task-form__input-error-activo');
		template += `<li>Campo marca : incorrecto</li> `;
		$('#container').html(template);
		campos['marca'] = false;
	}else {
		document.getElementById(`grupo__marca`).classList.remove('task-form__grupo-incorrecto');
		document.getElementById(`grupo__marca`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__marca .task-form__input-error`).classList.remove('task-form__input-error-activo');
		template += `<li>Campo marca : correcto</li> `;
		$('#container').html(template);
		campos['marca'] = true;
	}
}

const validarNombre = () =>{
	const nombre = document.getElementById("name");
	if(nombre.value == "" || nombre.value==null){
		document.getElementById(`grupo__nombre`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__nombre`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__nombre .task-form__input-error`).classList.add('task-form__input-error-activo');
		template += `<li>Campo nombre : incorrecto</li>`;
		$('#container').html(template);
		campos['nombre'] = false;
	}else {
		document.getElementById(`grupo__nombre`).classList.remove('task-form__grupo-incorrecto');
	    document.getElementById(`grupo__nombre`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__nombre .task-form__input-error`).classList.remove('task-form__input-error-activo');
		template += `<li>Campo nombre : correcto</li>`;
		$('#container').html(template);
		campos['nombre'] = true;
	}
}

const validarModelo = () =>{
	const modelo = document.getElementById("modelo");
	if(modelo.value == "" || modelo.value==null){
		document.getElementById(`grupo__modelo`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__modelo`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__modelo .task-form__input-error`).classList.add('task-form__input-error-activo');
		template += `<li>Campo modelo : incorrecto</li>`;
		$('#container').html(template);
		campos['modelo'] = false;
	}else {
		document.getElementById(`grupo__modelo`).classList.remove('task-form__grupo-incorrecto');
		document.getElementById(`grupo__modelo`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__modelo .task-form__input-error`).classList.remove('task-form__input-error-activo');
		template += `<li>Campo modelo : correcto</li>`;
		$('#container').html(template);
		campos['modelo'] = true;
	}
}


const validaPrecio = () =>{
	const precioP =document.getElementById('precio');
	if(precioP.value == null || precioP.value <= 99.98 || precioP.value==""){
		document.getElementById(`grupo__precio`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__precio`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__precio .task-form__input-error`).classList.add('task-form__input-error-activo');
		template += `<li>Campo precio : incorrecto</li>`;
		$('#container').html(template);
		campos['precio'] = false;
	}else {
		document.getElementById(`grupo__precio`).classList.remove('task-form__grupo-incorrecto');
		document.getElementById(`grupo__precio`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__precio .task-form__input-error`).classList.remove('task-form__input-error-activo');
		template += `<li>Campo precio : correcto</li>`;
		$('#container').html(template);
		campos['precio'] = true;
	}
}

const validaDetalles = () =>{
	const detalles =document.getElementById('detalles').value.length;

	if(detalles>250 ){
		document.getElementById(`grupo__detalles`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__detalles`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__detalles .task-form__input-error`).classList.add('task-form__input-error-activo');
        template += `<li>Campo detalles : incorrecto</li>`;
		$('#container').html(template);
		campos['detalles'] = false;
	}else {
		document.getElementById(`grupo__detalles`).classList.remove('task-form__grupo-incorrecto');
		document.getElementById(`grupo__detalles`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__detalles .task-form__input-error`).classList.remove('task-form__input-error-activo');
		template += `<li>Campo detalles : correcto</li>`;
		$('#container').html(template);
        campos['detalles'] = true;
	}
}

const validaUnidad = () =>{
	const unidad =document.getElementById('unidades');
	if(isNaN(unidad.value) == "" || isNaN(unidad.value) ==null || unidad.value != isNaN(unidad.value)){
		document.getElementById(`grupo__unidades`).classList.add('task-form__grupo-incorrecto');
		document.getElementById(`grupo__unidades`).classList.remove('task-form__grupo-correcto');
		document.querySelector(`#grupo__unidades .task-form__input-error`).classList.add('task-form__input-error-activo');
		template += `<li>Campo unidades : incorrecto</li> `;
		$('#container').html(template);
		campos['unidades'] = false;
	}else {
		document.getElementById(`grupo__unidades`).classList.remove('task-form__grupo-incorrecto');
		document.getElementById(`grupo__unidades`).classList.add('task-form__grupo-correcto');
		document.querySelector(`#grupo__unidades .task-form__input-error`).classList.remove('task-form__input-error-activo');
		template += `<li>Campo unidades : correcto</li>`;
		$('#container').html(template);
		campos['unidades'] = true;
	}
}


inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});

select.forEach((select) => {
	select.addEventListener('keyup', validarFormulario);
	select.addEventListener('blur', validarFormulario);
});

formulario.addEventListener('submit', (e) => {
	e.preventDefault();
	alert(campos.marca);
	//todos los campos deben ser true para que se envie el formulario
	if(campos.marca && campos.nombre && campos.modelo && campos.precio  && campos.unidades){
		formulario.reset();

		document.getElementById('task-form__mensaje-exito').classList.add('task-form__mensaje-exito-activo');
		//muestra el mensaje de exito por segundos donde:
		//1 segundo = 1000 
		setTimeout(() => {
			document.getElementById('task-form__mensaje-exito').classList.remove('task-form__mensaje-exito-activo');
		}, 10000);

		document.querySelectorAll('.task-form__grupo-correcto').forEach((icono) => {
			icono.classList.remove('task-form__grupo-correcto');
		});
	} else {
		document.getElementById('task-form__mensaje').classList.add('task-form__mensaje-activo');
	}
});